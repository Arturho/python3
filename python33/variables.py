#curso_codigo_facilito = "Python 3 desde ubuntu 18.4"
#curso_codigo_facilito = 300
#curso_codigo_facilito = True
#print( curso_codigo_facilito )

nuber_one = 10
nuber_two = 10

#resultado = nuber_one + nuber_two
#print( "Suma: ", resultado)

#resultado = nuber_one - nuber_two
#print( "Resta: ", resultado)

#resultado = nuber_one * nuber_two
#print( "Multiplicacion: ", resultado)

#resultado = nuber_one / nuber_two
#print( "Division: ", resultado)

#resultado = nuber_one // nuber_two
#print( "Division: ", resultado)

##Strings
#course = "python3"
#name = "Pablo"

#final_message = "Nuevo curso de" + course + " por " + name
#final_message = "Nuevo curso de %s por %s" %(course, name)
#final_message = "Nuevo curso de {} por {}".format(course, name)

#print(final_message)
edition = "Primer"
course = "Curso"
my_string = "codigo facilito!"
result = "{a} {b} de {c}".format(a=edition, b=course, c=my_string)
result = result.upper()
result = result.title();

#print(my_string)
#print(my_string[0:16])
#print(my_string[::-1])
#print(course[19])

## Metodos aplicables a strings ##
#print(result.lower())
#print(result.title())
#print(result.find('codigo'))
#print(result.count('C'))
#print(result.replace('P', 'F'))
#print(result.split(' '))

#trabajmdo con listas
#my_list = ["string", 15, 18.9, True]
my_list = [18, 55, 2, 1, 23, 5, 8]
my_list22 = [22, 23]
#my_list.append(6)
#my_list.insert(0, "insert")

#remover datos de una llista
#my_list.remove(15)
#remover ultimo valor de lista
#my_list.pop()

##ordenar listas
#my_list.sort(reverse=True)
##unir listas
#my_list.extend(my_list22)
#my_list.append(my_list22)
#print(my_list)

## trabajando con tuplas ##(son inmutables)(control total sobre datos)
#my_tuple = (1, "string", True)
#print(my_tuple)

## trabajando con diccionarios (no se rige por indices)##
diccionario = { 'a' : True, 5 : 'esto es un string', (1,2):False, 'a':100 }

diccionario['c'] = 'Nuevo string' #crear clave valor
diccionario['a'] = False #si se encuentra la llave lo actualiza , si no la crea

#valor =  diccionario['z']
valor =  diccionario.get('z', "Falsicimo") #encontar un valor en diccionario
##del diccionario[5] #Eliminar un clave

llaves = tuple(diccionario.keys())
valores = tuple(diccionario.values())

diccionario_dos = { 'z':23, 'w':88, 'curso':'Codigo super facilito'}
#diccionario['z'] = diccionario_dos['z']
#diccionario['w'] = diccionario_dos['w']
#diccionario.update(diccionario_dos) #unir dos diccionarios

#print(diccionario)
#print(valor)
#print(llaves)
#print(valores)


## trabjando con condcionales ##
'''fruta = 'manzana'

if fruta != 'kiwi':
    #print('La fruta es un kiwi')
    print('Es dferente')

elif fruta== 'manzana':
    print('Es una manzana')

elif fruta == 'pera':
    pass #ayuda a no marcar las excepciones

else:
    print('La fruta es'+ fruta)'''

#mensaje = 'El valor es kkiwi' if fruta == 'kiwi' else 'No son iguales'

#print(mensaje)

### variables nullas o vacias siempre van hacer alsa: None, [], (), {}, 0, '', falso
'''valor = [1]
valor2 = None

if valor or valor2 != None : #podemos validar 1 o mas variables [ and , or]
    print('verdadero')
else:
    print('falso')'''

###trabajando con while ####
'''contador = 0
bandera = True
while bandera: #contador < 10:
    print(contador) 
    contador+=1

    if contador == 5:
        print('Estamos en el numero 5')
        continue

    if contador == 6:
        #break
        bandera = False
    

else:
    print('El contador a finalizado')'''

### Trabjando con el ciclo FOR ###
'''lista = [1,2,3,4,5,6,7,8,9,10]

for valor in lista:
    ##print(valor)
    pass

#equivalente a for (var n= 0; n<20; n+5)
nuevo_rango = range(0, 20, 5)

for valor in nuevo_rango:
    #print(valor)
    pass

#indice = 0
#for valor in lista:
for indice,valor in enumerate(lista): #forma de obtener el indice y el valor en ciclo for
    pass #print(valor, " - indice ", indice)
    indice +=1


#print(len(lista))
for valor in range(0 , len(lista)):
    pass #print(valor)

## recorriendo un string
for valor in "curso de codigo facilito":
    #print(valor)
    pass


## recorrer un dicionario
diccionario= {'a' : 10, 'b' : 20, 'c' : 500}
for llave, valor in diccionario.items():
    print('La llave ', llave , 'tiene el valor de ', valor)'''


## trabajando con list comprehesion ###
'''lista = [] #False
for valor in range(0, 101):
    lista.append(valor)

print(lista)
lista = [ valor for valor in range(0, 101) if valor%2 ==0] #lo recomendable
# valor a agagregar en la lista
# un ciclo, for

tupla = tuple((valor for valor in range(0, 101) if valor%2 !=0))#obtener nmero impares desde una tupla
#print(tupla)


diccionario ={ indice:valor for indice , valor in enumerate(lista) if indice < 10}
print(diccionario)'''



###Trabajndo con funciones

def factorial_numero( numero ):
    #numero = 3
    factorial = 1
    while numero > 0:
        factorial =  factorial * numero
        numero -=1
    #print(factorial)
    return factorial
    #pass

#print(factorial_numero(5))

def suma(numero1, numero2): 
    resultado = numero1 +numero2

    #print(resultado)

    return resultado

    return 1

#print("El resultado de la suma es:" , suma(10, 550))

def division(valor1 , valor2):
    resultado = valor1/valor2

    return resultado

result =  division(100,20) #Forma 1
result =  division(valor2=20, valor1=100) #Forma 2(cuando sepamos a ciencia cierta el numero y nombre de los argumentos)

#print("El resultado de la division es:", result)

def multiplicacion(valor_uno=2, valor_dos=1):
    result = valor_uno*valor_dos
    return result


res = multiplicacion(1, 8)
#print("El resultado de la multiplicacion es:", res)

def multiples_valores():
    return "String", 1, True, 25.6 #Esto se retora como una tupla

#result = multiples_valores()
string, number, booel, flot = multiples_valores()
#print(multiples_valores()[0])##imprimir un valor de acuerdo a la pocion en la tupla

##Asignacion de una funcion a una variable
mi_variable = multiplicacion
res = mi_variable(6,6)
#print("El valor de la multiplicacion es:", res)

#Lamar funcion en otra funcion => ayudara a entender decoradores y lamdas
def mostrar_resultado( funcion):
    #print( funcion(6, 8))
    pass

mi_variable = multiplicacion
mi_variable_dos = suma
mostrar_resultado(mi_variable_dos)


#Variables locales y globales en una fuuncion
def palindromo():
    #frase = frase.replace(' ', '') #Variable local
    nuevo_valor = frase.replace(' ', '')
    #print(nuevo_valor)
    return nuevo_valor == nuevo_valor[::-1]

frase = 'anita lava la tina' #Variable global
#print(frase)
result = palindromo()
#print(frase)
#print(result)


##Cambiar el valor de una variable global
def valor_global():
    global variable_global #indicar que es global desde una funcion
    variable_global = 'Cambiar valor' #Variable local

def mostrar_global():
    #print(variable_global)
    pass
variable_global = 'Esto es una variable global'
#print(variable_global)

mostrar_global()
valor_global()

#print(variable_global)

##una funcion puede crear una variable global
def crear_global():
    global nueva_variable
    nueva_variable = 'Es una variable global'

crear_global()
#print(nueva_variable)


#Argumentos
def suma(*argumento):
    #print(argumento)
    #print(type(argumento)) #Imprimir el tio de dato
    result = 0

    for valor in argumento:
        result = result + valor

    return result


resultado = suma(3,4)
#print(resultado)

#crear funcion con argumentos especificos
def suma(**kwargs):
    #print(kwargs)
    #print(type(kwargs)) #Imprimir el tio de dato
    valor = kwargs.get('valors', 'No contine valor')
    #print(valor)
    

#* -> n valores => tuplas
#** -> n valores => dicciionarios
resultado = suma(valor = 'Nombre', x=2, z=True)
#print(resultado)


##Lambdas sirve para crear funcones anonimas
def suma(numero1, numero2): 
    resultado = numero1 +numero2

    return resultado

mi_funcion  = lambda numero1, numero2 : numero1 + numero2
formato = lambda sentencia : '¿{}?'.format(sentencia)
no_valor = lambda : 100
#no_resultado = lambda : print('Debe de ejecutar una accion')


resultado = mi_funcion(100, 200)
resultado = formato('Esta es una pregunta')
resultado = no_valor()
#resultado = no_resultado()

#print(resultado)

def decorador(is_valid = False):
    
    def _decorador(func):#A,B
        def before_action():
            print("Vamos a ajecutar la funcion")

        def after_action():
            pass
            #print("Se ejecuto la funcion")

        def nueva_funcion(*args, **kwargs):
            if is_valid:
                before_action()
            resultado_funcion= func(*args, **kwargs)
            after_action()

            return resultado_funcion
        return nueva_funcion #C
    return _decorador
#Decoradores
#A, B, C son funcones
#A recie como parametro B para crear a C
#Los decoradores pueden recibir paramtros
@decorador(is_valid = True)
def saluda():
    print("Hola mundo")

@decorador()
def sumas(num1, num2):
    #print(num1+num2)
    return num1+num2

re = sumas(80,17)
#print(re)
#saluda()

###generadores##Sirven oara poder crear objetos sin necesidad de guardarlos en memoria
##Siempre se utiliza la palabra reservada yield
def return_valores():
    print("ola mundo")
    return True


def generador(*args):
    for valor in args:
        yield valor * 10, True
    #pass

for valor_uno, valor_dos in generador(1,2,3,4,55,6,7,8,9):
    pass
    #print(valor_uno, valor_dos)
#print(return_valores())



###docstring
def generator(*args):
    '''recibe n cantidad de numeros y regresa el numero ademas de regresar True o False si el nuero es mayor que 5'''
    for valor in args:
        yield valor * 10, True if valor>5 else False
    #pass
documentacion  = generator.__name__ #__doc__

##print(documentacion)

#Modulos()

