class Animal:
    @property
    def terrestre(self):
        return True

class Felino(Animal):#Clase padre
    @property
    def garras(self):
        return True

    def cazar(self):
        print("El felino esta cazando")

class Gato(Felino):
    def gato_cazador(self):
        self.cazar()

class Jaguar(Felino):
    pass

gato = Gato()
jaguar = Jaguar()

print(gato.gato_cazador())
print(gato.terrestre)
print(jaguar.terrestre)