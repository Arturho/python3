class Circulo:
    #__pi = 3.1416
    @staticmethod
    def pi(): #Metodos Estaticos
        return 3.1416

    def __init__(self, radio):
        self.radio = radio

    def area(self): #etodos de instancia
        return self.radio * self.radio * self.pi()

    


circulo_uno = Circulo(7)
print(circulo_uno.area())
         