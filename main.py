#from calculadora import __name__ as name_calculadora

#print(__name__)
#print(name_calculadora)#cuando el script sea el principal

###Formas de impportar modulos

#import calculadora
#from calculadora import suma, multiplicacion, division
#from calculadora import resta as r1
#from calculadora import *

#resultado = r1(10, 20)

#print(resultado)

import random
import datetime
import sys
import time

valor = random.randint(0, 10)
lista = [True, 20, "Strings", False]
valor = random.choice(lista)
random.shuffle(lista)

#print(datetime.datetime.now())
for i in range(100):
    time.sleep(0.5)
    sys.stdout.write("\r%d %%" % i)
    sys.stdout.flush()


'''def f(x):
    return x % 2 != 0 == 0 and x % 3 !=0


lista = filter(f, range(2,25))

print(lista)'''
