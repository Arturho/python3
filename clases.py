#Propierties

class Usuario:
    def __init__(self, username, password, email):
        self.username = username
        self.__password = self.__generar_password(password) #atributo privado
        self.email = email

    def __generar_password(self, password):
        return password.upper()

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self , valor):
        self.__password = self.__generar_password(valor)

arturo = Usuario("Arturo", "calabaza90", "cruzjart@gmail.com")

print(arturo.password)
arturo.password = 'Nuevo valor'
print(arturo.password)



#Variables de clase
'''class Circulo:

    __pi = 3.1416 #variable de clase, son cosntantes se deben anteponer guion bajo para identificar que no es modifcable

    def __init__(self, radio):
        self.radio = radio
    
    def area(self):
        return self.radio *  self.radio * Circulo.__pi


circulo_uno = Circulo(4)
circulo_dos = Circulo(3)


#print(Circulo.pi)
#print(circulo_uno.pi)
#print(circulo_dos.pi)
#print(circulo_uno.__dict__)
print(circulo_uno.area())'''


#Metodos privados
'''class Usuario:
    def __init__(self, username, password, email):
        self.username = username
        self.__password = self.__generar_password(password) #atributo privado
        self.email = email

    def __generar_password(self, password):
        return password.upper()

    def get_password(self):
        return self.__password

#crear una instancia de usuario
arturo = Usuario("Arturo", "calabaza90", "cruzjart@gmail.com")
print(arturo.username)
print(arturo.get_password()) #Forma de obtner un atributo privado(siilar a java)
print(arturo.email)'''


#init
'''class Lapiz:
    

    def __init__(self, color="Rojo", contiene_borrador=True, usa_grafito=False):
        self.color = color
        self.contiene_borrador = contiene_borrador
        self.usa_grafito = usa_grafito

    def dibujar(self):
        print('El lapiz esta dibujando')

    def borrar(self):
        if self.puede_borrar():
            print('El lapiz puede borrrar')
        else:
            print('El lapiz no puede borrar')

    def puede_borrar(self):
        return self.contiene_borrador

#objeto
lapiz_generico = Lapiz()
lapiz_generico.dibujar()
#lapiz_generico.puede_borrar()
lapiz_generico.borrar()'''