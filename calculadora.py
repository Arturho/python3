#!/usr/bin/python3
"""
Aqui colocamos todo lo que hace el modulo
"""

__author__ ="Arturo Cruz"
__copyright__ ="Copyright 2019"
__credits__ ="Free coding"

__license__ = "GPL"
__version__ = "1.0.1"
__mainteiner = "Arturo Cruz"
__email__ = "cruzjart@gmail.com"
__status__ = "Developer"

def suma(valor_uno, valor_dos):
    ''' funcon uncon que suma dos valores'''
    return valor_uno+valor_dos


def resta(valor_uno, valor_dos):
    ''' funcion que resta dos valores'''
    return valor_uno - valor_dos


def multiplicacion(valor_uno, valor_dos):
    '''funcion que multiplica dos valores'''
    return valor_uno* valor_dos


def division(valor_uno, valor_dos):
    '''funcion que divide dos calores'''
    return valor_uno/valor_dos

def saluda():
    print("Saluda")
#print(__name__)

if __name__ == '__main__':
    saluda()